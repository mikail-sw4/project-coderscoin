# About me

I'm a blockchain learning tool that allows you to visualize and understand every step in the blockchain process. From generating keys, executing transactions, and mining blocks, to visualizing the entire blockchain network with its forks, this tool is designed to make learning about blockchain easy and interactive. <br>
Made by a group of 3 passionate developers for CODERS.BAY Vienna used for teaching purposes.

Work by Mikail Yagci:
- Frontend
- File Handling (upload and download in specific JSON formats)
- User control (login, register, logout)
- Security (Spring-Security and Web-Security)

Used in this project:
- **Vue.JS** (HTML/CSS/JavaScript), **SpringBoot** (Java), **MySQL**, **AntV-G6** (graph visualization engine (https://g6.antv.vision/en))

# How to work with me

- Mysql or H2
    - To use H2, change the corresponding properties in the `application.properties`-file 
- Install NodeJS & NPM

## Frontend

- Open Project in terminal and run `npm install` and `npm run serve`
- Open `http://localhost:3000` in any browser

## Backend 

- Download Java 17
- Open Project in IntelliJ
- Run defined run-script `Main`
- If everything works correctly, the backend should be available on `http://localhost:8080`

# Use Cases

### 1. Create an account:
- your following inputs in ***/register*** don't affect the tool as all transactions are anonymous
- creating an account gives you access to your wallet
    - your wallet will manage all of your key pairs, give free starting coins, and you will be able to add and generate key pairs
- creating an account isn't necessary, but recommended for this tool

### 2. Collect your 10 free coins:
- in order to collect your free coins, go to ***/wallet***, click on the _free coins_ button and a JSON file will be downloaded
    - the JSON file contains transactions which has to be mined
- in order to mine the transaction, go to ***/mining***, upload the transaction and select or enter your public key for collecting reward
    - you can leave the default values for _difficulty_ and _nonce_
- your block will be saved as you click on _mine_. If you like to share your block, click on _export_

### 3. Make a transaction:
- navigate to ***/transaction***
- select or enter the public keys of the sender and receiver
    - you may choose one of your own keys as the receiver
        - you can generate keys on ***/wallet*** via clicking on _generate_
- enter the amount of coins
- in order to complete a transaction, you need to sign it. This is done with the respective private key of the sender
    - the private key will be added automatically if choose to select the public key via dropdown menu, otherwise you have to type it manually
- click _send_ and the transaction is ready to be exported

### 4. Save transactions into the blockchain:
- in order to save transactions, you will need to mine the downloaded transaction JSON file
- navigate to ***/mining***
- upload the transaction and select or enter your public key for collecting reward
    - you can leave the default values for _difficulty_ and _nonce_
- your block will be saved as you click on _mine_. If you like to share your block, click on _export_

### 5. Mine manually:
- navigate to ***/mining***
- upload the transaction and select or enter your public key for collecting reward
- adjust the value for _difficulty_. The _difficulty_ corresponds to the number of leading zeros in the hash
- enter arbitrary numbers into the _nonce_ field and wait until the hash of the block has been deemed valid
- a block is stored in the blockchain once the hash is valid
    - to bypass interactive mode, click _mine_. A fitting _nonce_ will be generated for you
- if you like to share your block, click on _export_

### 6. Working with the dashboard:
- navigate to ***/dashboard***
- the dashboard consists of three components
    - a table of blocks, a table of transactions and a chart of public key connections 
        - the block- and transaction-tables list entities already within the blockchain
        - the chart shows a force-directed graph where the nodes represent wallets and the edges represent transactions between them. The size of a node is directly proportional to the balance of the wallet. Edges get shorter the more frequent transactions happen between wallets

### 7. Working with the blockchain graph:
- navigate to ***/blockchain***
- all blocks are shown as a tree, starting from the genesis block
- for more detail, click on any block, you will be directed to a separate view
    - the separate view will give you a deep overview of the block, the transactions and mining reward included in it
- to add an external block which is not yet included in your local blockchain, click _import_ or drag the JSON file onto the graph
    - the imported will be added to the blockchain permanently

# UI

![Landing Page](Frontend/src/assets/MD_images/LandingPage.png)
![Dashboard](Frontend/src/assets/MD_images/Dashboard.png)
![Wallet](Frontend/src/assets/MD_images/WalletPage.png)
![Mining](Frontend/src/assets/MD_images/MiningPage.png)