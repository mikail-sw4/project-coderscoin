import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import router from './router'
import store from './store'


Vue.use(VueAxios, axios)


Vue.config.productionTip = false


async function createVueApp() {
  await store.dispatch('getSignedInUser')
  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app')
}
createVueApp()