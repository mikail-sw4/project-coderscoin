package main.login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
@Transactional
public class UserRepository {

    @Autowired
    UserCRUDRepository userCRUDRepository;

    //Registrierung
    //überprüft, ob der Name schon vergeben wurde - falls nicht, speichert neue User in DB
    public boolean insertNewUser(String name, String password, String firstName, String lastName){
        Optional<User> existingUser = userCRUDRepository.findByUserName(name);
        if(existingUser.isPresent()) {
            return false;
        }
        userCRUDRepository.save(new User(name, password, firstName, lastName));
        return true;
    }

    public User findByName(String name) {
        return userCRUDRepository.findByUserName(name).orElse(null);
    }

}
