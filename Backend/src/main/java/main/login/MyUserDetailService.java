package main.login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

@Service
@Transactional
public class MyUserDetailService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByName(username); //find User with name "username" in our database
        if (user == null) {
            throw new UsernameNotFoundException("No user found with username:" + username);
        }
        ArrayList<GrantedAuthority> authorities = new ArrayList<>();

        //hard coding a role for every user. (could also be retrieved from the database for advanced
        // authority-management)
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));

        //create spring security user with values from our user
        org.springframework.security.core.userdetails.User springSecUser =
                new org.springframework.security.core.userdetails.User(user.getUserName(), (user.getPassword()),
                authorities);
        return springSecUser;
    }

    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder(); //NoOpPasswordEncoder.getInstance();
    }
}
