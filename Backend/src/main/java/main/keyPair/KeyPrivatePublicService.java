package main.keyPair;

import main.block.BlockService;
import main.login.UserCRUDRepository;
import main.util.Utils;
import org.bitcoinj.core.Base58;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.security.*;
import java.security.interfaces.ECPrivateKey;
import java.util.ArrayList;
import java.util.List;

@Service
public class KeyPrivatePublicService {
    @Autowired
    private KeyPrivatePublicRepository keyPrivatePublicRepository;
    @Autowired
    private UserCRUDRepository userCRUDRepository;
    @Autowired
    BlockService blockService;

    public KeyPrivatePublic getKeyPairGenerator(int idUser) throws NoSuchAlgorithmException {
        //create key pair
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("EC");
        keyPairGenerator.initialize(256);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        PrivateKey privateKey = keyPair.getPrivate();
        ECPrivateKey ecPrivateKey = (ECPrivateKey)privateKey;

        //convert private key to hexadecimal format
        String privateKeyString = Utils.adjustTo64(ecPrivateKey.getS().toString(16).toUpperCase());

        return new KeyPrivatePublic(privateKeyString, userCRUDRepository.findById(idUser).get());
    }

    public String convertWifPrivateKeyToHexPrivateKey(String wifPrivateKey) throws NoSuchAlgorithmException {
        if(wifPrivateKey.length() == 51){
            String hexPrivateKey = Utils.bytesToHex(Base58.decode(wifPrivateKey));
            hexPrivateKey = hexPrivateKey.substring(2, hexPrivateKey.length()-8).toUpperCase();

            String privateKeyWif = hexPrivateKey;
            privateKeyWif = "80" + privateKeyWif;
            byte[] byteShaing = MessageDigest.getInstance("SHA-256").digest(Utils.hexStringToByteArray(privateKeyWif));
            byteShaing = MessageDigest.getInstance("SHA-256").digest(byteShaing);
            privateKeyWif = privateKeyWif +  Utils.bytesToHex(byteShaing).substring(0,8);
            privateKeyWif =  Base58.encode(Utils.hexStringToByteArray(privateKeyWif));

            if (wifPrivateKey.equals(privateKeyWif)) {
                return hexPrivateKey;
            }
            else{
                return "false";
            }
        }
        else {
            String hexPrivateKey = Utils.bytesToHex(Base58.decode(wifPrivateKey));
            hexPrivateKey = hexPrivateKey.substring(2, hexPrivateKey.length()-10).toUpperCase();

            String privateKeyWif = hexPrivateKey;
            privateKeyWif = "80" + privateKeyWif + "01";
            byte[] byteShaing = MessageDigest.getInstance("SHA-256").digest(Utils.hexStringToByteArray(privateKeyWif));
            byteShaing = MessageDigest.getInstance("SHA-256").digest(byteShaing);
            privateKeyWif = privateKeyWif +  Utils.bytesToHex(byteShaing).substring(0,8);
            privateKeyWif =  Base58.encode(Utils.hexStringToByteArray(privateKeyWif));

            if (wifPrivateKey.equals(privateKeyWif)) {
                return hexPrivateKey;
            }
            else{
                return "false";
            }
        }

    }

    public List<KeysWithFundsDTO> getKeysAndFunds(int idUser) throws NoSuchAlgorithmException {
        List<KeysWithFundsDTO> keysAndFundsList = new ArrayList<>();

        for (KeyPrivatePublic keyPrivatePublic : keyPrivatePublicRepository.findByUser(userCRUDRepository.findById(idUser).get())){
            double amount = 0;
            amount = amount + blockService.getFundsFromChain(keyPrivatePublic.getPublicKeyBase58(), blockService.getLongestChainFromTheBlockchain());
            keysAndFundsList.add(new KeysWithFundsDTO(keyPrivatePublic.getPrivateKeyWif(), keyPrivatePublic.getPublicKeyBase58(), amount));
        }
        return keysAndFundsList;
    }

    public double getFundsPerAccount(int idUser) throws NoSuchAlgorithmException {
        double value = 0;
        for (KeysWithFundsDTO keysWithFundsDTO : getKeysAndFunds(idUser)) {
            value = value + keysWithFundsDTO.amount;
        }
        return value;
    }

    public List<KeyPrivatePublicDTO> allKeyPairsByUser(int idUser) {
        try{
            List<KeyPrivatePublicDTO> keyPrivatePublicDTOList = new ArrayList<>();
            for (KeyPrivatePublic k: keyPrivatePublicRepository.findByUser(userCRUDRepository.findById(idUser).get())) {
                keyPrivatePublicDTOList.add(new KeyPrivatePublicDTO(k.getIdKeyPrivatePublic(), k.getPublicKeyBase58(), k.getPrivateKeyWif()));
            }
            return keyPrivatePublicDTOList;
        }
        catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "no keys for this user found", e);
        }
    }

    public List<KeyPrivatePublicDTO> keyPairList(KeyPrivatePublic keyPrivatePublic, int idUser) throws NoSuchAlgorithmException {
        for (KeyPrivatePublic k: keyPrivatePublicRepository.findByUser(userCRUDRepository.findById(idUser).get())) {
            if (k.getPrivateKeyWif().equals(keyPrivatePublic.getPrivateKeyWif())) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "private key already saved");
            }
        }
        keyPrivatePublicRepository.save(keyPrivatePublic);
        List<KeyPrivatePublicDTO> keyPrivatePublicDTOList = new ArrayList<>();
        for (KeyPrivatePublic k: keyPrivatePublicRepository.findByUser(userCRUDRepository.findById(idUser).get())) {
            keyPrivatePublicDTOList.add(new KeyPrivatePublicDTO(k.getIdKeyPrivatePublic(), k.getPublicKeyBase58(), k.getPrivateKeyWif()));
        }
        return keyPrivatePublicDTOList;
    }

    public List<KeyPrivatePublicDTO> keyPairManually(int idUser, String wifPrivateKey) throws NoSuchAlgorithmException {
        //checking if private key is already saved
        for (KeyPrivatePublic k: keyPrivatePublicRepository.findByUser(userCRUDRepository.findById(idUser).get())) {
            if (k.getPrivateKeyWif().equals(wifPrivateKey) || k.getPrivateKeyWifCompressed().equals(wifPrivateKey)) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "private key already saved");
            }
        }

        String privateKey = "";
        if (wifPrivateKey.length() != 51 && wifPrivateKey.length() != 52) {
            if (wifPrivateKey.length() < 51) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "not a valid key (to short)");
            }else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "not a valid key (to long)");
            }
        }

        if (!wifPrivateKey.matches("[1-9A-HJ-NP-Za-km-z]+")) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "not a valid key (not a WIF)");
        }
        else {
            privateKey = convertWifPrivateKeyToHexPrivateKey(wifPrivateKey);
            if (privateKey.equals("false")) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "not a valid key (checksum incorrect)");
            }
            else {
                keyPrivatePublicRepository.save(new KeyPrivatePublic(privateKey, userCRUDRepository.findById(idUser).get()));
                List<KeyPrivatePublicDTO> keyPrivatePublicDTOList = new ArrayList<>();
                for (KeyPrivatePublic k: keyPrivatePublicRepository.findByUser(userCRUDRepository.findById(idUser).get())) {
                    keyPrivatePublicDTOList.add(new KeyPrivatePublicDTO(k.getIdKeyPrivatePublic(), k.getPublicKeyBase58(), k.getPrivateKeyWif()));
                }
                return keyPrivatePublicDTOList;
            }
        }
    }

    public void firstKeyPairGenrator(int idUser) throws NoSuchAlgorithmException {
        //generating the first keypair if you register
        KeyPrivatePublic keyPrivatePublic = getKeyPairGenerator(idUser);
        for (KeyPrivatePublic k: keyPrivatePublicRepository.findByUser(userCRUDRepository.findById(idUser).get())) {
            if (k.getPrivateKeyWif().equals(keyPrivatePublic.getPrivateKeyWif())) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "private key already saved");
            }
        }
        keyPrivatePublicRepository.save(keyPrivatePublic);
    }
}
