package main.keyPair;

import com.fasterxml.jackson.annotation.JsonBackReference;
import main.login.User;

import main.util.Utils;
import org.bitcoinj.core.Base58;
import org.bitcoinj.core.ECKey;

import javax.persistence.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


@Entity
public class KeyPrivatePublic {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idKeyPrivatePublic;

    @ManyToOne
    @JsonBackReference(value = "keyPrivatePublicList")
    @JoinColumn(name= "id_user")
    private User user;

    private String privateKey;

    public KeyPrivatePublic() {}

    public KeyPrivatePublic(String privateKey, User user) {
        this.privateKey = privateKey;
        this.user = user;
    }

    public Long getIdKeyPrivatePublic() {
        return idKeyPrivatePublic;
    }

    public void setIdKeyPrivatePublic(Long idKeyPrivatePublic) {
        this.idKeyPrivatePublic = idKeyPrivatePublic;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getPrivateKeyWif() throws NoSuchAlgorithmException {
        //convert hexadecimal private key to WIF private key
        String privateKeyWif = this.privateKey;
        privateKeyWif = "80" + privateKeyWif;
        byte[] byteShaing = MessageDigest.getInstance("SHA-256").digest(Utils.hexStringToByteArray(privateKeyWif));
        byteShaing = MessageDigest.getInstance("SHA-256").digest(byteShaing);
        privateKeyWif = privateKeyWif +  Utils.bytesToHex(byteShaing).substring(0,8);
        privateKeyWif =  Base58.encode(Utils.hexStringToByteArray(privateKeyWif));
        return privateKeyWif;
    }

    public String getPrivateKeyWifCompressed() throws NoSuchAlgorithmException {
        //convert hexadecimal private key to compressed WIF private key
        String privateKeyWifCompressed = this.privateKey;
        privateKeyWifCompressed = "80" + privateKeyWifCompressed + "01";
        byte[] byteShaing = MessageDigest.getInstance("SHA-256").digest(Utils.hexStringToByteArray(privateKeyWifCompressed));
        byteShaing = MessageDigest.getInstance("SHA-256").digest(byteShaing);
        privateKeyWifCompressed = privateKeyWifCompressed +  Utils.bytesToHex(byteShaing).substring(0,8);
        privateKeyWifCompressed =  Base58.encode(Utils.hexStringToByteArray(privateKeyWifCompressed));
        return privateKeyWifCompressed;
    }

    public String getPublicKey(){
        //get hexadecimal public key 130chars  from private key hexadecimal
        return ECKey.fromPrivate(Utils.hexStringToByteArray(this.privateKey)).decompress().getPublicKeyAsHex().toUpperCase();
    }

    public String getPublicKeyCompressed(){
        //get public key (66chars) from private key hexdecimal
        return Utils.bytesToHex(ECKey.fromPrivate(Utils.hexStringToByteArray(this.privateKey)).getPubKey()).toUpperCase();
    }

    public String getPublicKeyBase58(){
        return Base58.encode(ECKey.fromPrivate(Utils.hexStringToByteArray(this.privateKey)).getPubKey());
    }
}
