package main.transaction;

import com.fasterxml.jackson.annotation.JsonBackReference;
import main.block.Block;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Transaction {
    @Id
    @Column(nullable = false)
    private String signedTransaction;


    private String publicKeyWithdrawal;
    private String publicKeyDeposit;
    private double amount;
    private LocalDateTime timestamp;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name="id_block")
    private Block block;

    public Transaction() {
    }

    public Transaction(String publicKeyWithdrawal, String publicKeyDeposit, double amount) {
        this.publicKeyWithdrawal = publicKeyWithdrawal;
        this.publicKeyDeposit = publicKeyDeposit;
        this.amount = amount;
    }

    public String getSignedTransaction() {
        return signedTransaction;
    }

    public void setSignedTransaction(String signedTransaction) {
        this.signedTransaction = signedTransaction;
    }

    public String getPublicKeyWithdrawal() {
        return publicKeyWithdrawal;
    }

    public void setPublicKeyWithdrawal(String publicKeyWithdrawl) {
        this.publicKeyWithdrawal = publicKeyWithdrawl;
    }

    public String getPublicKeyDeposit() {
        return publicKeyDeposit;
    }

    public void setPublicKeyDeposit(String publicKeyDeposit) {
        this.publicKeyDeposit = publicKeyDeposit;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public Block getBlock() {
        return block;
    }

    public void setBlock(Block block) {
        this.block = block;
    }

    public String toStringForMining(){
        return "" + signedTransaction.toUpperCase() + publicKeyWithdrawal.toUpperCase() + publicKeyDeposit.toUpperCase() + amount + timestamp;
    }
}
