package main.blockchainVisual;

import main.block.Block;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Node {
    Block block;
    Collection<Node> children;

    public String getTitle() throws NoSuchAlgorithmException {
        return "Block " + block.getIdBlockForHash();
    }

    public List<NodeContent> getContent() throws NoSuchAlgorithmException {
        List<NodeContent> nodeContentList = new ArrayList<>();
        if (block.getIdBlockForHash() == 0){
            nodeContentList.add(new NodeContent("Nonce: ", ""));
            nodeContentList.add(new NodeContent("Hash: ", block.getHashed().substring(0,8) + "..." + block.getHashed().substring(50,block.getHashed().length())));
            nodeContentList.add(new NodeContent("PrevHash: ", ""));
            nodeContentList.add(new NodeContent("Time: ", ""));
            return nodeContentList;
        }
        nodeContentList.add(new NodeContent("Nonce: ", block.getNonce().toString()));
        nodeContentList.add(new NodeContent("Hash: ", block.getHashed().substring(0,8) + "..." + block.getHashed().substring(50,block.getHashed().length())));
        nodeContentList.add(new NodeContent("PrevHash: ", block.getPreviousHash().substring(0,8) + "..." + block.getPreviousHash().substring(50,block.getPreviousHash().length())));
        nodeContentList.add(new NodeContent("Time: ", block.getTimestamp() + ""));
        return nodeContentList;
    }

    public void setBlock(Block block) {
        this.block = block;
    }

    public void setChildren(Collection<Node> children) {
        this.children = children;
    }

    public Block getBlock() {
        return block;
    }

    public Collection<Node> getChildren() {
        return children;
    }

    public Node(Block block, Collection<Node> children) {
        this.block = block;
        this.children = children;
    }
}
