package main.block;

import main.blockchainVisual.Node;
import main.transaction.Transaction;
import main.transaction.TransactionRepository;
import main.util.Utils;
import org.bitcoinj.core.Base58;
import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.xml.bind.DatatypeConverter;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static main.util.Utils.genesisBlockHash;
import static main.util.Utils.masterKeyPublic;

@Service
public class BlockService {
    @Autowired
    private BlockRepository blockRepository;
    @Autowired
    private TransactionRepository transactionRepository;


    public Block mineBlock(List<Transaction> transactionList, long difficulty, String rewardPublicKey) throws NoSuchAlgorithmException {
        long latestBlockIdForHash = 0;
        String latestBlockHash = genesisBlockHash;

        //check if transactions are valid
        checkIfTransactionsAreValid(transactionList, getLongestChainFromTheBlockchain());

        //adding mining reward to block
        transactionList.add(addMiningRewardToBlock(rewardPublicKey));

        //sorting the transaction list
        Collections.sort(transactionList, new Comparator<Transaction>() {
            @Override
            public int compare(Transaction o1, Transaction o2) {
                return o1.getSignedTransaction().compareTo(o2.getSignedTransaction());
            }
        });

        //checking if transaction is in the blockchain + getting the latest block from the blockchain
        checkIfTransactionIsInTheBlockchain(transactionList);
        if (getLastBlockFromLongestChain() != null) {
            latestBlockIdForHash = getLastBlockFromLongestChain().getIdBlockForHash();
            latestBlockHash = getLastBlockFromLongestChain().getHashed();
        }

        //create block
        Block block = new Block(transactionList, latestBlockIdForHash + 1, latestBlockHash);
        block.setTransactionList(transformTransactionListToUpdatedListWithBlockId(transactionList, block));

        //mining the block with brute force
        block.setNonce(startMiningWithBruteForce(difficulty, block));
        block.setBlockCorrectMined(true);

        //save block and transactions
        blockRepository.save(block);
        transactionRepository.saveAll(transactionList);

        return block;
    }

    public Block mineBlockWithNonce(List<Transaction> transactionList, BigInteger nonce, long difficulty, String rewardPublicKey) throws NoSuchAlgorithmException {
        long latestBlockIdForHash = 0;
        String latestBlockHash = genesisBlockHash;

        //check if transactions are valid
        checkIfTransactionsAreValid(transactionList, getLongestChainFromTheBlockchain());

        //adding mining reward to block
        transactionList.add(addMiningRewardToBlock(rewardPublicKey));

        //sorting the transaction list
        Collections.sort(transactionList, new Comparator<Transaction>() {
            @Override
            public int compare(Transaction o1, Transaction o2) {
                return o1.getSignedTransaction().compareTo(o2.getSignedTransaction());
            }
        });

        //checking if transaction is in the blockchain + getting the latest block from the blockchain
        checkIfTransactionIsInTheBlockchain(transactionList);
        if (getLastBlockFromLongestChain() != null) {
            latestBlockIdForHash = getLastBlockFromLongestChain().getIdBlockForHash();
            latestBlockHash = getLastBlockFromLongestChain().getHashed();
        }

        //create block
        Block block = new Block(transactionList, latestBlockIdForHash + 1, latestBlockHash);
        block.setTransactionList(transformTransactionListToUpdatedListWithBlockId(transactionList, block));
        block.setNonce(nonce);

        //check if block is mined correct with the difficulty
        block.setBlockCorrectMined(checkIfBlockIsCorrectMined(difficulty, hashBlockWithNonce(block, nonce)));

        return block;
    }

    public void checkIfTransactionsAreValid(List<Transaction> transactionList, List<Block> blockChainToCheckTransaction) {
        for (Transaction transaction : transactionList) {
            if(!transaction.getPublicKeyWithdrawal().equals(masterKeyPublic)){
                if(getFundsFromChain(transaction.getPublicKeyWithdrawal(), blockChainToCheckTransaction) < transaction.getAmount()){
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Not enough funds for transaction available");
                }
            }
            else{
                for (Block block : blockChainToCheckTransaction) {
                    for (Transaction transaction1 : block.getTransactionList()) {
                        if(transaction1.getPublicKeyDeposit().equals(transaction.getPublicKeyDeposit())){
                            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User already earned his free coins!");
                        }
                    }
                }
            }
            // creating Sha object from transaction
            Sha256Hash hash = Sha256Hash.wrap(Utils.calculateHash(transaction));

            byte[] res;
            try{
                //Convert Hashed transaction (from Hex to Bytes) for verifying
                res = DatatypeConverter.parseHexBinary(transaction.getSignedTransaction());
            }catch (Exception e){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Transaction", e);
            }

            // verify transaction
            boolean b;
            try {
                b = ECKey.verify(hash.getBytes(), ECKey.ECDSASignature.decodeFromDER(res), Base58.decode(transaction.getPublicKeyWithdrawal()));
            } catch (Exception e) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Transaction", e);
            }
            if (!b) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Transaction");
            }
        }
    }

    private byte[] hashBlockWithNonce(Block block, BigInteger nonce) throws NoSuchAlgorithmException {
        String transactionAreInString = transformTransactionListToStringForHash(block);
        String hashedStringFromBlock = block.getIdBlockForHash() + block.getPreviousHash() + transactionAreInString + block.getTimestamp() + nonce;
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        return digest.digest(hashedStringFromBlock.getBytes(StandardCharsets.UTF_8));
    }

    private boolean checkIfBlockIsCorrectMined(long difficulty, byte[] hash) {
        String hashDifficulty = "";
        for (long i = 0; i < difficulty; i++) {
            hashDifficulty = hashDifficulty + "0";
        }
        return Utils.bytesToHex(hash).substring(0, (int) difficulty).equals(hashDifficulty);
    }

    private Transaction addMiningRewardToBlock(String rewardPublicKey) {
        Transaction rewardTransaction = new Transaction("Mining-Reward", rewardPublicKey, 2);
        rewardTransaction.setTimestamp(LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS));
        rewardTransaction.setSignedTransaction("Mining-Reward-" + rewardPublicKey + "-" + rewardTransaction.getTimestamp());
        return rewardTransaction;
    }

    private BigInteger startMiningWithBruteForce(long difficulty, Block block) throws NoSuchAlgorithmException {
        String hashDifficulty = "";
        boolean checkIfHashed = true;
        BigInteger nonce = BigInteger.valueOf(0);

        String transactionAreInString = transformTransactionListToStringForHash(block);

        for (long i = 0; i < difficulty; i++) {
            hashDifficulty = hashDifficulty + "0";
        }

        while (checkIfHashed) {
            nonce = nonce.add(BigInteger.valueOf(1));
            String hashedStringFromBlock = block.getIdBlockForHash() + block.getPreviousHash() + transactionAreInString + block.getTimestamp() + nonce;
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(hashedStringFromBlock.getBytes(StandardCharsets.UTF_8));
            if (Utils.bytesToHex(hash).substring(0, (int) difficulty).equals(hashDifficulty)) {
                checkIfHashed = false;
            }
        }
        return nonce;
    }

    private void checkIfTransactionIsInTheBlockchain(List<Transaction> transactionList) throws NoSuchAlgorithmException {
        Block blockToReturn = new Block();
        for (Block block : getLongestChainFromTheBlockchain()) {
            if (block != null && blockToReturn.getIdBlockForHash() < block.getIdBlockForHash()) {
                blockToReturn = block;
            }
            if (block != null) {
                for (Transaction transactionInBlockchain : block.getTransactionList()) {
                    for (Transaction transactionFromFrontend : transactionList) {
                        if (transactionInBlockchain.getSignedTransaction().equals(transactionFromFrontend.getSignedTransaction()) && transactionInBlockchain.getBlock() != null) {
                            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Transaction already in the Blockchain");
                        }
                    }
                }
            }
        }
    }

    private String transformTransactionListToStringForHash(Block block) {
        String transactionAreInString = "";
        for (Transaction transaction : block.getTransactionList()) {
            transactionAreInString = transactionAreInString + transaction.toStringForMining();
        }
        return transactionAreInString;
    }

    private List<Transaction> transformTransactionListToUpdatedListWithBlockId(List<Transaction> transactions, Block block) {
        List<Transaction> transactionList = new ArrayList<>();
        for (Transaction transaction : transactions) {
            transaction.setBlock(block);
            transactionList.add(transaction);

        }
        return transactionList;
    }

    public void blockVerification(Block block) throws NoSuchAlgorithmException {
        if (block.getPreviousHash().equals(genesisBlockHash)) {
            return;
        }
        for (Block blockDB : blockRepository.findAll()) {
            if (blockDB.getHashed().equals(block.getPreviousHash()) && !blockDB.getHashed().equals(block.getHashed())) {
                return;
            }
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Block can´t be attached to the blockchain ");
    }

    public List<Block> getLongestChainFromTheBlockchain() throws NoSuchAlgorithmException {
        List<Block> chain = new ArrayList<>();
        if (blockRepository.count() == 0) {
            return chain;
        }
        chain.add(blockRepository.findByIdBlockForHash(blockRepository.getLastBlock()).get(0));

        long t = chain.get(0).getIdBlockForHash();
        int i = 0;
        while (t > 0) {
            for (Block block : blockRepository.findByIdBlockForHash(t - 1)) {
                if (block.getHashed().equals(chain.get(i).getPreviousHash())) {
                    chain.add(block);
                }
            }
            t--;
            i++;
        }
        return chain;
    }

    public Block getLastBlockFromLongestChain() {
        if (blockRepository.count() == 0) {
            return null;
        }

        List<Block> blockList = blockRepository.findByIdBlockForHash(blockRepository.getLastBlock());
        Collections.sort(blockList, new Comparator<Block>() {
            @Override
            public int compare(Block o1, Block o2) {
                return o1.getTimestamp().compareTo(o2.getTimestamp());
            }
        });

        return blockList.get(0);
    }

    public Node getBlockchain() throws NoSuchAlgorithmException {
        List<Block> blockList = (List<Block>) blockRepository.findAll();
        return resolve(blockList);
    }

    private Node resolve(List<Block> db) throws NoSuchAlgorithmException {
        /* O(n) */
        Map<String, Set<Block>> tree = new HashMap<>();
        for (Block block : db)
            tree.put(block.getHashed(), new HashSet<>());
        tree.put(genesisBlockHash, new HashSet<>());
        for (Block b : db)
            tree.get(b.getPreviousHash()).add(b);
        return resolve(Block.getGenesis(), tree);
    }

    private Node resolve(Block b, Map<String, Set<Block>> tree) throws NoSuchAlgorithmException {
        List<Node> list = new ArrayList<>();
        for (Block c : tree.get(b.getHashed()))
            list.add(resolve(c, tree));
        return new Node(b, list);
    }

    public void doubleBlockCheck(Block block) throws NoSuchAlgorithmException {
        for (Block blockFromChain: blockRepository.findAll()){
            if (blockFromChain.getHashed().equals(block.getHashed())){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Block already in the Blockchain");
            }
        }
    }

    public List <Transaction> getTransactionsByTime () throws NoSuchAlgorithmException {
        List<Transaction> transactionList = new ArrayList<>();
        for (Block block:getLongestChainFromTheBlockchain()) {
            for (Transaction transaction:block.getTransactionList()) {
                if (!transaction.getSignedTransaction().substring(0,6).equals("Mining")) {
                    transactionList.add(transaction);
                }
            }
        }

        Collections.sort(transactionList, new Comparator<Transaction>() {
            @Override
            public int compare(Transaction o1, Transaction o2) {
                return -o1.getTimestamp().compareTo(o2.getTimestamp());
            }
        });
        return transactionList;
    }

    public double getFundsFromChain(String publicKey, List<Block> blockChainToCheckTransaction) {
        double funds = 0;
        for (Block block : blockChainToCheckTransaction) {
            for (Transaction transaction : block.getTransactionList()) {
                if (transaction.getPublicKeyDeposit().equals(publicKey)) {
                    funds = funds + transaction.getAmount();
                }
                if (transaction.getPublicKeyWithdrawal().equals(publicKey)) {
                    funds = funds - transaction.getAmount();
                }
            }
        }
        return funds;
    }


    public List<Block> getCorrectChain(Block blockFromThisGetChain,List<Block> blockList) throws NoSuchAlgorithmException {
        for (Block block : blockRepository.findAll()) {
            if (block.getHashed().equals(blockFromThisGetChain.getPreviousHash())) {
                blockList.add(block);
                if (block.getPreviousHash().equals(genesisBlockHash)) {
                    return blockList;
                }
                else {
                    getCorrectChain(block, blockList);
                }
            }
        }
        return blockList;
    }

    public void saveBlocks(List<Block> blockList) throws NoSuchAlgorithmException {
        int blocksTryToAdd = blockList.size();
        //filter list
        List<Block> newBlockList = new ArrayList<>();
        for (Block block : blockList) {
            block.setIdBlock(null);
            if (block.getPreviousHash().equals(genesisBlockHash)) {
                newBlockList.add(block);
            }
            else {
                if (getCorrectChain(block, new ArrayList<>()).size() > 0) {
                    newBlockList.add(block);
                }
                else{
                    for (Block block1 : blockList) {
                        if(block.getPreviousHash().equals(block1.getHashed())){
                            newBlockList.add(block);
                        }
                    }
                }
            }
        }
        int blocksAdded = newBlockList.size();

        if (newBlockList.size() == 0) {
            blocksAdded= 0;
        }

        //sort list by id
        Collections.sort(newBlockList, new Comparator<Block>() {
            @Override
            public int compare(Block o1, Block o2) {
                return Long.compare(o1.getIdBlockForHash(), o2.getIdBlockForHash());
            }
        });


        //check blocks if they are allowed to be added
        List<ResponseStatusException> errors = new ArrayList<>();
        for (Block block : newBlockList) {
            try {
                doubleBlockCheck(block);
                blockVerification(block);
                List<Transaction> transactionList = new ArrayList<>();
                for (Transaction transaction : block.getTransactionList()) {
                    if (!transaction.getPublicKeyWithdrawal().equals("Mining-Reward")) {
                        transactionList.add(transaction);
                    }
                }

                checkIfTransactionsAreValid(transactionList, getCorrectChain(block, new ArrayList<>()));
                blockRepository.save(block);
                for (Transaction transaction : block.getTransactionList()) {
                    transaction.setBlock(block);
                    transactionRepository.save(transaction);
                }
            } catch (ResponseStatusException e) {
                errors.add(e);
            }
        }
        if (!errors.isEmpty()) {
            blocksAdded = blocksAdded - errors.size();
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, blocksAdded + "/" + blocksTryToAdd);
        }
    }
}
