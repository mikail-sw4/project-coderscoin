package main.block;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BlockRepository extends CrudRepository<Block, Long> {
    List<Block> findByIdBlockForHash(long idBlockForHash);
    Block findByIdBlock(long idBlock);

    @Query("SELECT max(idBlockForHash) FROM Block")
    long getLastBlock();

}
